import React from 'react';
import './App.css';
import withFirebaseAuth from 'react-with-firebase-auth';
import LeadList from './component/leadList/leadList';
import { firebaseConfig } from './database/firebaseConfig';
import firebase from 'firebase';
import Header from './component/header/header';
import { initializeIcons } from '@uifabric/icons';
import { PrimaryButton, Text, Spinner, SpinnerSize, Dialog, DialogType, DialogFooter, DefaultButton } from 'office-ui-fabric-react';
import { getCurrentDate } from './component/utility';
import GrantPermission from './component/permission/grantPermission';
import CardList from './component/cards/cardlist';

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAppAuth = firebaseApp.auth();
const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider(),
};


class App extends React.Component {

  constructor(props) {
    super(props);
    initializeIcons();
    this.state = {
      activeOption: "leads",
      showSignoutDialog: false,
    }
  }

  render() {
    const { user, signInWithGoogle } = this.props;

    if (user && !this.state.firebaseUser) {
      this.fetchUserFromFirebase(user);
      return (<div>
        <Spinner size={SpinnerSize.large} label="Connecting to server..." ariaLive="assertive" />
      </div>)
    }
    else if (user && this.state.firebaseUser) {
      if (this.state.firebaseUser.userType > 1) {
        return (
          <div className="App" >
            <Header user={user} onClick={this.OnHeaderIconClicked.bind(this)} activeOption={this.state.activeOption} />
            <hr style={{ margin: "0px" }} firebaseUser={this.state.firebaseUser} />
            {this.renderComponent(this.state.activeOption)}
            {this.renderSignoutDialog(this.state.showSignoutDialog)}
          </div>
        );
      } else {
        return (
          <div>
            <Text variant={'large'} >You dont have any permission to view this dashboard</Text>
            <PrimaryButton onClick={() => { this.props.signOut() }}>SignOut</PrimaryButton>
          </div>
        );
      }
    }
    else {
      return (
        <div className="App" style={{ textAlign: "center", padding: "50px" }} >
          <Text variant={'large'} >Welcome to Mitra</Text><br />
          <PrimaryButton onClick={signInWithGoogle}>Sign in with Google</PrimaryButton>
        </div>
      )
    }
  }

  renderComponent(option) {
    switch (option) {
      case "leads": {
        return (<LeadList user={this.state.firebaseUser} />);
      }
      case "permissions": {
        return (<GrantPermission user={this.state.firebaseUser} />);
      }
      case "cards": {
        return (<CardList user={this.state.firebaseUser} />);
      }
    }
  }

  renderSignoutDialog(show) {
    if (show) {
      return (<Dialog
        hidden={false}
        onDismiss={() => { this.setState({ showSignoutDialog: false }); }}
        dialogContentProps={{
          type: DialogType.normal,
          title: 'Signout',
          closeButtonAriaLabel: 'Close',
          subText: 'Do you want to signout ?'
        }}
      >
        <DialogFooter>
          <PrimaryButton onClick={() => { this.props.signOut(); this.setState({ showSignoutDialog: false }); }} text="Signout" />
          <DefaultButton onClick={() => { this.setState({ showSignoutDialog: false }); }} text="Cancel" />
        </DialogFooter>
      </Dialog>);
    } else {
      return null;
    }
  }

  OnHeaderIconClicked(option) {
    if (option == "signout") {
      this.setState({ showSignoutDialog: true });

    } else {
      this.setState({ activeOption: option });
    }
  }

  // Network call
  fetchUserFromFirebase(user) {
    firebase.database().ref("/user/" + user.uid).once('value').then((snapshot) => {
      if (snapshot.val()) {
        this.state.firebaseUser = snapshot.val();
        this.setState(this.state);
      } else {
        let userObj = {
          createdon: getCurrentDate(),
          email: user.email,
          experienced: true,
          id: user.uid,
          image: user.photoURL,
          income: "",
          mobile: "",
          mobileVerified: false,
          name: user.displayName,
          occupation: "",
          userType: 1
        }
        firebase.database().ref("/user/" + user.uid).set(userObj, () => {
          this.state.firebaseUser = userObj;
          this.setState(this.state);
        });
      }
    });
  }
}
export default withFirebaseAuth({
  providers,
  firebaseAppAuth,
})(App);
