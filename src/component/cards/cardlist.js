import React from 'react';
import firebase from 'firebase';
import { DefaultButton, PrimaryButton, Text, Panel, Stack, TextField, Dropdown, PanelType, Spinner, DetailsList, ShimmeredDetailsList, SelectionMode, DetailsListLayoutMode, Image } from 'office-ui-fabric-react';
import { getCurrentDate } from '../utility';

class CardList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showCreateCard: false,
            newCard: {},
            error: {},
            savingCard: false,
            productList: [],
            columns: this.getColumns()
        };
    }

    componentDidMount() {
        this.fetchProducts();
    }
    render() {
        return (
            <div>
                {this.renderCreatePanel()}
                <div style={{ width: "100%", display: "inline-block" }}>
                    <Text variant={'large'} style={{ margin: "10px", float: "left" }} >Cards List</Text>
                    <PrimaryButton style={{ margin: "10px", float: "right" }} text="Add New Card" onClick={() => { this.setState({ showCreateCard: true }); }} />
                </div>
                <div style={{ width: "100%" }}>
                    {this.renderProductList()}
                </div>
            </div>

        );
    }



    renderCreatePanel() {
        if (this.state.showCreateCard) {
            return (
                <Panel
                    type={PanelType.custom}
                    isLightDismiss
                    isOpen={true}
                    onDismiss={() => { this.setState({ showCreateCard: false, newCard: {} }) }}
                    headerText="Add New Card"
                    closeButtonAriaLabel="Close"
                    customWidth="600px"
                    onRenderFooterContent={() => {
                        let footerElement;
                        if (!this.state.savingCard) {
                            footerElement = <div>
                                <PrimaryButton onClick={() => { this.AddNewCard() }} text="Save" />
                                <DefaultButton onClick={() => { this.setState({ showCreateCard: false, newCard: {} }) }} text="Cancel" style={{ marginLeft: "10px" }} />
                            </div>
                        }
                        else {
                            footerElement = <div><Spinner /></div>;
                        }

                        return (
                            <div>
                                {footerElement}
                            </div>
                        );
                    }}
                    isFooterAtBottom={true}
                >
                    <Stack horizontal>
                        <Stack vertical style={{ padding: "10px", width: "300px" }}>
                            <Dropdown
                                label="Bank"
                                required
                                options={BankList}
                                errorMessage={this.state.error.bank}
                                disabled={false}
                                defaultSelectedKey={this.state.newCard.bank}
                                onChange={(ev, option) => {
                                    this.updateNewCardObject("bank", option.key);
                                }}
                            />

                            <TextField
                                label="Card Name"
                                description="Enter card name as per bank"
                                required
                                errorMessage={this.state.error.name}
                                value={this.state.newCard.name}
                                onChange={(ev, newValue) => {
                                    this.updateNewCardObject("name", newValue);
                                }}
                            />

                            <TextField
                                label="Minimum Income Required"
                                required
                                errorMessage={this.state.error.income}
                                value={this.state.newCard.income ?? 0}
                                description="Only numbers less than 1000000"
                                onChange={(ev, newValue) => {
                                    if (newValue == "") {
                                        this.updateNewCardObject("income", 0)
                                    }
                                    else if (parseInt(newValue) && parseInt(newValue) < 1000000) {
                                        this.updateNewCardObject("income", parseInt(newValue));
                                    } else {
                                        this.updateNewCardObject("income", this.state.newCard.income)
                                    }
                                }}
                            />

                            <TextField
                                label="Annual Fees"
                                value={this.state.newCard.fees ?? 0}
                                description="Only numbers less than 100000, if free card leave it 0"
                                onChange={(ev, newValue) => {
                                    if (newValue == "") {
                                        this.updateNewCardObject("fees", 0)
                                    }
                                    else if (parseInt(newValue) && parseInt(newValue) < 1000000) {
                                        this.updateNewCardObject("fees", parseInt(newValue));
                                    } else {
                                        this.updateNewCardObject("fees", this.state.newCard.fees)
                                    }
                                }}
                            />

                            <TextField
                                label="Rating"
                                value={this.state.newCard.rating ?? 0}
                                description="0-5 just for sorting(5 is best)"
                                onChange={(ev, newValue) => {
                                    if (newValue == "") {
                                        this.updateNewCardObject("rating", 0)
                                    }
                                    else if (parseInt(newValue) && parseInt(newValue) < 1000000) {
                                        this.updateNewCardObject("rating", parseInt(newValue));
                                    } else {
                                        this.updateNewCardObject("rating", this.state.newCard.rating)
                                    }
                                }}
                            />

                            <Dropdown
                                placeholder="Select features"
                                label="Features"
                                multiSelect
                                options={FeatureList}
                                onChange={(ev, option) => { this._onFeaturesDropdownChange(option) }}
                            />

                        </Stack>
                        <Stack vertical style={{ padding: "10px", width: "300px" }}>
                            <TextField
                                label="Bank Icon Url"
                                value={this.state.newCard.bankicon}
                                onChange={(ev, newValue) => {
                                    this.updateNewCardObject("bankicon", newValue);
                                }}
                            />

                            <TextField
                                label="Card Icon Url"
                                description="This image will show up in app in card list"
                                value={this.state.newCard.cardicon}
                                onChange={(ev, newValue) => {
                                    this.updateNewCardObject("cardicon", newValue);
                                }}
                            />

                            <Dropdown
                                required
                                label="Category"
                                options={CategoryList}
                                errorMessage={this.state.error.category}
                                disabled={false}
                                defaultSelectedKey={this.state.newCard.category}
                                onChange={(ev, option) => {
                                    this.updateNewCardObject("category", option.key);
                                }}
                            />

                            <TextField
                                label="Description"
                                required
                                errorMessage={this.state.error.description}
                                description="Will show up as summary about card"
                                value={this.state.newCard.description}
                                onChange={(ev, newValue) => {
                                    this.updateNewCardObject("description", newValue);
                                }}
                            />

                            <TextField
                                label="Extra Description HTML"
                                multiline
                                description="will show to up when user click on more details"
                                value={this.state.newCard.extradescription}
                                onChange={(ev, newValue) => {
                                    this.updateNewCardObject("extradescription", newValue);
                                }}
                            />
                        </Stack>
                    </Stack>
                </Panel>
            );
        }
    }

    renderProductList() {
        if (!this.state.productList.length) {
            return (
                <ShimmeredDetailsList
                    setKey="items"
                    items={this.state.productList}
                    compact={false}
                    columns={this.state.columns}
                    selectionMode={SelectionMode.none}
                    enableShimmer={true}
                    listProps={{ renderedWindowsAhead: 0, renderedWindowsBehind: 0 }}
                />)

        } else {
            return (<DetailsList
                items={this.state.productList}
                compact={false}
                columns={this.state.columns}
                selectionMode={SelectionMode.none}
                getKey={this._getKey}
                setKey="none"
                layoutMode={DetailsListLayoutMode.justified}
                isHeaderVisible={true}
            />);
        }
    }

    updateNewCardObject(key, value) {
        let newCard = this.state.newCard;
        newCard[key] = value;
        this.setState({ newCard: newCard });
    }

    AddNewCard() {
        let newCard = this.state.newCard;
        let error = this.state.error;
        error.bank = newCard.bank ? "" : "Select bank";
        error.name = newCard.name ? "" : "Enter card name";
        error.income = newCard.income && newCard.income != 0 ? "" : "Income cannot be 0";
        error.category = newCard.category ? "" : "Select category";
        error.description = newCard.description ? "" : "Enter some description";

        let isError = false;
        Object.entries(error).forEach(
            ([, value]) => { if (value != "") isError = true; }
        );

        this.setState({ error: error });

        if (!isError) {
            newCard.product = "CREDIT CARD";
            newCard.status = "ACTIVE";
            newCard.createdon = getCurrentDate();
            this.setState({ savingCard: true });
            firebase.firestore().collection("products").add(newCard)
                .then(() => {
                    this.setState({ savingCard: false, newCard: {}, error: {}, showCreateCard: false })
                });
        }

    }

    fetchProducts() {
        let db = firebase.firestore();
        let query;
        query = db.collection("products");
        query.onSnapshot((snapshot) => {
            let products = [];
            snapshot.forEach((doc) => {
                products.push(doc.data());
            });
            this.setState({ productList: products });
        });
    }
    _onColumnClick(column) {
        const newColumns = this.state.columns;
        const currColumn = newColumns.filter(currCol => column.key === currCol.key)[0];
        newColumns.forEach((newCol) => {
            if (newCol === currColumn) {
                currColumn.isSortedDescending = !currColumn.isSortedDescending;
                currColumn.isSorted = true;
            } else {
                newCol.isSorted = false;
                newCol.isSortedDescending = true;
            }
        });
        const newItems = this._copyAndSort(this.state.productList, currColumn.fieldName, currColumn.isSortedDescending);
        this.setState({
            columns: newColumns,
            productList: newItems
        });
    };

    _copyAndSort(items, columnKey, isSortedDescending) {
        const key = columnKey;
        return items.slice(0).sort((a, b) => ((isSortedDescending ? String(a[key]).toLowerCase() < String(b[key]).toLowerCase() : String(a[key]).toLowerCase() > String(b[key]).toLowerCase()) ? 1 : -1));
    }

    _onFeaturesDropdownChange(option) {
        let featuresArr = this.state.newCard.features;
        if (!featuresArr) featuresArr = [];
        if (option.selected) {
            featuresArr.push(option.key);
        } else {
            var index = featuresArr.indexOf(option.key);
            if (index > -1) {
                featuresArr.splice(index, 1);
            }
        }
        this.updateNewCardObject("features", featuresArr);
    }


    getColumns() {
        return [
            {
                name: 'Card Icon',
                key: 'cardicon',
                data: 'string',
                fieldName: 'cardicon',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <Image src={item.cardicon} alt="Invalid Image" width={50} />;
                }
            },
            {
                name: 'Name',
                key: 'name',
                data: 'string',
                fieldName: 'name',
                minWidth: 100,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.name}</span>;
                }
            },
            {
                name: 'Bank Icon',
                key: 'bankicon',
                data: 'string',
                fieldName: 'bankicon',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <Image src={item.bankicon} alt="Invalid Image" width={50} />;
                }
            },
            {
                name: 'Bank',
                key: 'bank',
                data: 'string',
                fieldName: 'bank',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.bank}</span>;
                }
            },
            {
                name: 'Income',
                key: 'income',
                data: 'string',
                fieldName: 'income',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.income}</span>;
                }
            },
            {
                name: 'Fees',
                key: 'fees',
                data: 'string',
                fieldName: 'fees',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.fees}</span>;
                }
            },
            {
                name: 'Created On',
                key: 'createdon',
                data: 'string',
                fieldName: 'createdon',
                minWidth: 100,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.createdon}</span>;
                }
            },
            {
                name: 'Product',
                key: 'product',
                data: 'string',
                fieldName: 'product',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return (<span>{item.product}</span>);
                }
            },
            {
                name: 'Category',
                key: 'category',
                data: 'string',
                fieldName: 'category',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return (<span>{item.category}</span>);
                }
            },
            {
                name: 'Features',
                key: 'features',
                data: 'string',
                fieldName: 'features',
                minWidth: 100,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    let a = [];
                    if (item.features) {
                        item.features.forEach(element => {
                            a.push(<span>{element}</span>);
                        });
                        return (<Stack vertical>
                            {a}
                        </Stack>)
                    }
                }
            },
            {
                name: 'Description',
                key: 'description',
                data: 'string',
                fieldName: 'description',
                minWidth: 150,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return (<span>{item.description}</span>);
                }
            },
            {
                name: 'Extra Description',
                key: 'extradescription',
                data: 'string',
                fieldName: 'extradescription',
                minWidth: 150,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return (<span>{item.extradescription}</span>);
                }
            },
            {
                name: 'Status',
                key: 'status',
                data: 'string',
                fieldName: 'status',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.status}</span>;
                }
            },
        ]
    }



}

const BankList = [
    { key: 'HDFC', text: 'HDFC' },
    { key: 'SBI', text: 'SBI' },
    { key: 'ICICI', text: 'ICICI' },
    { key: 'PNB', text: 'PNB' },
    { key: 'AXIS', text: 'AXIS' },
    { key: 'CANARA', text: 'CANARA' },
    { key: 'BOB', text: 'BOB' },
    { key: 'UNION', text: 'UNION' },
    { key: 'YES', text: 'YES' },
    { key: 'IDFC', text: 'IDFC' },
    { key: 'RBL', text: 'RBL' },
    { key: 'KOTAK', text: 'KOTAK' },
    { key: 'INDUSIND', text: 'INDUSIND' }
];

const CategoryList = [
    { key: 'TRAVEL', text: 'TRAVEL' },
    { key: 'SHOPPING', text: 'SHOPPING' },
    { key: 'DINING', text: 'DINING' },
    { key: 'LIFESTYLE', text: 'LIFESTYLE' }
];

const FeatureList = [
    { key: 'LIFE TIME FREE', text: 'LIFE TIME FREE' },
    { key: 'FIRST YEAR FREE', text: 'FIRST YEAR FREE' },
    { key: 'FREE MOVIES', text: 'FREE MOVIES' },
    { key: 'FREE LOUNGE', text: 'FREE LOUNGE' }
];

export default CardList;