import React from 'react';
import { DefaultButton, PrimaryButton } from 'office-ui-fabric-react';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <div>
                <div style={{width:"100%", textAlign:"left"}} >
                    {this.props.activeOption == "leads"?
                        (<PrimaryButton style={{ margin: "10px", float:"inline-start" }} text="Leads" onClick={() => { this.props.onClick("leads") }} checked={true} />):(<DefaultButton style={{ margin: "10px", float:"inline-start" }} text="Leads" onClick={() => { this.props.onClick("leads") }} disabled={false} />)}
                    {this.props.activeOption == "permissions"?
                        (<PrimaryButton style={{ margin: "10px", float:"inline-start" }} text="Permissions" onClick={() => { this.props.onClick("permissions") }} checked={true} />):(<DefaultButton style={{ margin: "10px", float:"inline-start" }} text="Permissions" onClick={() => { this.props.onClick("permissions") }} disabled={false} />)}
                    {this.props.activeOption == "cards"?
                        (<PrimaryButton style={{ margin: "10px", float:"inline-start" }} text="Cards" onClick={() => { this.props.onClick("cards") }} checked={true} />):(<DefaultButton style={{ margin: "10px", float:"inline-start" }} text="Cards" onClick={() => { this.props.onClick("cards") }} disabled={false} />)}                    
                    
                    <DefaultButton style={{ margin: "10px", float: "right" }} text="SignOut" onClick={() => { this.props.onClick("signout") }} disabled={false} checked={this.props.activeOption == "signout"} />
                </div>
            </div>

        );
    }
        
}

export default Header;