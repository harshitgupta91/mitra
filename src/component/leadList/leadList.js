import React from 'react';
import firebase from 'firebase';
import { DetailsListLayoutMode, DetailsList, SelectionMode, Fabric, Stack, PrimaryButton, DefaultButton, ProgressIndicator, ShimmeredDetailsList, Dialog, DialogType, TextField, Spinner, SpinnerSize, Icon, ColorClassNames, DialogFooter, Text } from 'office-ui-fabric-react';
import { getCurrentDate, getStatus } from '../utility';
import LeadStatusDialog from './leadStatusDialog';

class LeadList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            timelineDialog: {
                visibility: false,
                leadId: "",
                createdon: ""
            }
        };
        this.fetchLeads();
    }

    render() {
        return (
            <div style={{ padding: "20px" }}>
                <Stack horizontal>
                    <Text variant={'large'} >Leads List</Text>
                </Stack>
                {this.state.timelineDialog.visibility ?
                    <LeadStatusDialog
                        leadDetails={this.state.timelineDialog}
                        onClose={this._closeDialog.bind(this)}
                    />
                    : ("")}
                {!this.state.items.length ? (
                    < ShimmeredDetailsList
                        setKey="items"
                        items={this.state.items}
                        compact={false}
                        columns={this.getColumns()}
                        selectionMode={SelectionMode.none}
                        enableShimmer={true}
                        listProps={{ renderedWindowsAhead: 0, renderedWindowsBehind: 0 }}
                    />) : (

                        <DetailsList
                            items={this.state.items}
                            compact={false}
                            columns={this.getColumns()}
                            selectionMode={SelectionMode.none}
                            getKey={this._getKey}
                            setKey="none"
                            layoutMode={DetailsListLayoutMode.justified}
                            isHeaderVisible={true}
                        />)
                }
            </div>
        );
    }


    _closeDialog() {
        let obj = this.state.timelineDialog;
        obj.visibility = false;
        this.setState({ timelineDialog: obj });
    }



    _onColumnClick(ev, column) {
        const newColumns = this.state.columns;
        const currColumn = newColumns.filter(currCol => column.key === currCol.key)[0];
        newColumns.forEach((newCol) => {
            if (newCol === currColumn) {
                currColumn.isSortedDescending = !currColumn.isSortedDescending;
                currColumn.isSorted = true;
            } else {
                newCol.isSorted = false;
                newCol.isSortedDescending = true;
            }
        });
        const newItems = this._copyAndSort(this.state.items, currColumn.fieldName, currColumn.isSortedDescending);
        this.setState({
            columns: newColumns,
            items: newItems
        });
    };

    _copyAndSort(items, columnKey, isSortedDescending) {
        const key = columnKey;
        return items.slice(0).sort((a, b) => ((isSortedDescending ? String(a[key]).toLowerCase() < String(b[key]).toLowerCase() : String(a[key]).toLowerCase() > String(b[key]).toLowerCase()) ? 1 : -1));
    }

    getColumns() {
        return [
            {
                name: 'Name',
                key: 'name',
                data: 'string',
                fieldName: 'name',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.name}</span>;
                }
            },
            {
                name: 'Mobile',
                key: 'mobile',
                fieldName: 'mobile',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.mobile}</span>;
                }
            },
            {
                name: 'Address',
                key: 'address',
                fieldName: 'address',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.address}</span>;
                }
            },
            {
                name: 'Pincode',
                key: 'pincode',
                fieldName: 'pincode',
                minWidth: 50,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.pincode}</span>;
                }
            },
            {
                name: 'Product',
                key: 'product',
                fieldName: 'product',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.product}</span>;
                }
            },
            {
                name: 'Income',
                key: 'income',
                fieldName: 'income',
                minWidth: 50,
                maxWidth: 50,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.income}</span>;
                }
            },
            {
                name: 'Age',
                key: 'age',
                fieldName: 'age',
                minWidth: 30,
                maxWidth: 30,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.age}</span>;
                }
            },
            {
                name: 'Created On',
                key: 'createdon',
                fieldName: 'createdon',
                minWidth: 50,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.createdon}</span>;
                }
            },
            {
                name: 'Status',
                key: 'status',
                fieldName: 'status',
                minWidth: 120,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return (
                        <Stack vertical>
                            <div style={{ textAlign: "left" }}>{getStatus(item.status)}</div>
                            <PrimaryButton style={{ fontSize: "10px", height: "20px", marginTop: "5px", width: "100px" }} text={getStatus(item.status + 1) ? "Change Status" : "Show Timeline"}
                                onClick={() => this.statusChangeButtonClicked(item.id, item.createdon)}
                                checked={false} />
                        </Stack>);
                }
            },
        ];
    }

    statusChangeButtonClicked(leadId, createdon) {
        this.setState({
            timelineDialog: {
                visibility: true,
                leadId: leadId,
                createdon: createdon
            }
        });
    }

    // Network Calls    

    fetchLeads() {
        let db = firebase.firestore();
        let query;
        if (this.props.user.userType == 2 && this.props.user.pincode) {
            query = db.collection("leads").where('pincode', 'in', this.props.user.pincode.split(','));
        } else {
            query = db.collection("leads");
        }
        query.onSnapshot((snapshot) => {
            let leads = [];
            snapshot.forEach((doc) => {
                leads.push(doc.data());
            });
            this.setState({ items: leads });
        });
    }

}
export default LeadList;