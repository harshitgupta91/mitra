import React from 'react';
import firebase from 'firebase';
import { DetailsListLayoutMode, DetailsList, SelectionMode, Fabric, Stack, PrimaryButton, DefaultButton, ProgressIndicator, ShimmeredDetailsList, Dialog, DialogType, TextField, Spinner, SpinnerSize, Icon, ColorClassNames, DialogFooter, Text, Modal, IconButton, CommandBarButton, SpinnerType, addElementAtIndex, Link } from 'office-ui-fabric-react';
import { getCurrentDate, getStatus } from '../utility';

class LeadStatusDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            upload: 0,
            items: [],
            descriptionEdit: {},
            uploadEdit: {}
        };
        this.fileInput = React.createRef();
        this.fetchLeadUpdateStatus(this.props.leadDetails.leadId);
    }
    render() {

        return (
            <Modal
                isOpen={true}
                onDismiss={this.props.onClose}
                isBlocking={false}
            >
                <Stack vertical style={{ margin: '20px', width: "900px" }} >
                    <div style={{ display: 'block' }}>
                        <div style={{ float: "left", fontSize: "20px", fontWeight: "bold" }}>Lead Timeline</div>
                        <IconButton
                            style={{ float: "right" }}
                            iconProps={{ iconName: 'Cancel' }}
                            ariaLabel="Close"
                            onClick={this.props.onClose}
                        />
                    </div>

                    {!this.state.items.length ? (
                        < ShimmeredDetailsList
                            setKey="items"
                            items={this.state.items}
                            shimmerLines={5}
                            compact={true}
                            columns={this.getColumns()}
                            selectionMode={SelectionMode.none}
                            enableShimmer={true}
                            listProps={{ renderedWindowsAhead: 0, renderedWindowsBehind: 0 }}
                        />) : (

                            <DetailsList
                                items={this.state.items}
                                compact={true}
                                columns={this.getColumns()}
                                selectionMode={SelectionMode.none}
                                getKey={this._getKey}
                                setKey="none"
                                layoutMode={DetailsListLayoutMode.justified}
                                isHeaderVisible={true}
                            />)
                    }
                </Stack>
            </Modal>
        );
    }

    _onPreviousFileUpload(file, docid, status) {
        let obj = this.state.uploadEdit;
        obj[docid] = true;
        this.setState({ uploadEdit: obj });
        this.uploadFile(file[0], status).then((file) => {
            file.ref.getDownloadURL().then((url) => {
                this.updateFileUrl(docid, url);
            });
        });
    }

    _onDialogFileUpload(file, status) {
        this.state.upload = 1;
        this.setState(this.state);
        this.uploadFile(file[0], status).then((file) => {
            file.ref.getDownloadURL().then((url) => {
                this.state.upload = 2;
                this.state.downloadUrl = url;
                this.setState(this.state);
            });
        });;
    }

    _onStatusCompleteClicked(newStatus) {
        this.changeLeadStatus(this.props.leadDetails.leadId, newStatus, this.state.description, this.state.downloadUrl);
    }

    _onDialogTextChange(ev, newText) {
        this.state.description = newText;
        this.setState(this.state);
    }


    getColumns() {
        let that = this;
        return [
            {
                name: 'Status',
                key: 'status',
                data: 'string',
                fieldName: 'status',
                minWidth: 150,
                maxWidth: 150,
                onRender: (item) => {
                    if (item.state == 1) {
                        return (<div style={{ fontWeight: "bold", fontSize: "16px", color: "rgb(0, 120, 212)" }}>{getStatus(item.status)}</div>);
                    } else {
                        return <span style={{ fontSize: "14px" }}>{getStatus(item.status)}</span>;
                    }
                    return
                }
            },
            {
                name: 'Stage Modified',
                key: 'changedon',
                fieldName: 'changedon',
                minWidth: 100,
                maxWidth: 100,
                onRender: (item) => {
                    if (item.state == 2 || item.state == 1) {
                        return <span>Pending</span>;
                    } else {
                        return <div>{item.changedon}</div>;
                    }

                }
            },
            {
                name: 'Description',
                key: 'description',
                fieldName: 'description',
                minWidth: 200,
                maxWidth: 200,
                onRender: (item) => {
                    if (item.status == 1) {
                        return <Text>No description</Text>;
                    }
                    else if (item.state == 1) {
                        return (<TextField
                            multiline
                            placeholder="Enter description"
                            onChange={this._onDialogTextChange.bind(this)} />);
                    } else if (item.state == 0) {
                        let saveElement;
                        if (this.state.descriptionEdit[item.id]) {
                            if (this.state.descriptionEdit[item.id].status == 0) {
                                saveElement = <IconButton iconProps={{ iconName: 'Save' }} disabled={false}
                                    onClick={() => {
                                        this.updateDescription(item.id, this.state.descriptionEdit[item.id].value);
                                    }} />;
                            }
                            else
                                saveElement = <Spinner size={SpinnerSize.small} style={{ marginLeft: "10px", marginRight: "2px", marginTop: "10px", alignSelf: "start" }} />
                        } else {
                            saveElement = <IconButton iconProps={{ iconName: 'Save' }} disabled={true} />;
                        }
                        return (<Stack horizontal>
                            <TextField
                                onChange={(ev, newVal) => {
                                    let obj = this.state.descriptionEdit;
                                    obj[item.id] = {
                                        value: newVal,
                                        status: 0
                                    }
                                    this.setState({ descriptionEdit: obj });
                                }}
                                multiline
                                value={this.state.descriptionEdit[item.id] ? this.state.descriptionEdit[item.id].value : item.description}
                            />
                            {saveElement}
                        </Stack>);
                    }

                }
            },
            {
                name: 'Attachments',
                key: 'attachments',
                fieldName: 'attachments',
                minWidth: 150,
                maxWidth: 150,
                onRender: (item) => {
                    if (item.state != 2) {
                        if (item.downloadUrl) {
                            var indents = [];
                            let i = 1;

                            if (Array.isArray(item.downloadUrl)) {
                                item.downloadUrl.forEach(element => {
                                    indents.push(<Link href={element} target="_blank"><Icon iconName="Download" style={{ fontSize: 10 }} ></Icon> Download File {i++}</Link>);
                                });
                            } else {
                                indents.push(<Link href={item.downloadUrl} target="_blank"><Icon iconName="Download" style={{ fontSize: 10 }} ></Icon> Download File {i++}</Link>);
                            }
                            return (<div>
                                <Stack vertical>
                                    {indents}
                                </Stack>
                            </div>);
                        } else {
                            return (<span> No Attachment</span>)
                        }
                    }
                }
            },
            {
                name: 'Upload File',
                key: 'file',
                fieldName: 'file',
                minWidth: 50,
                maxWidth: 100,
                onRender: (item) => {

                    if (item.status != 1) {
                        if (item.state == 0) {
                            let input;
                            if (this.state.uploadEdit[item.id]) {
                                input = <Spinner />
                            } else {
                                input = <div><input type="file" ref={this.fileInput} onChange={(e) => that._onPreviousFileUpload(e.target.files, item.id, item.status)} style={{ width: "90px" }} /></div>;
                            }
                            return input;
                        }
                        else if (item.state == 1) {
                            let progress, input, text;
                            if (this.state.upload) {
                                if (this.state.upload == 1) {
                                    progress = <Spinner size={SpinnerSize.small} />
                                }
                                else if (this.state.upload == 2) {
                                    text = <span className={ColorClassNames.green}>Upload Success</span>;
                                    progress = <Icon iconName="CheckMark" className={ColorClassNames.green} />
                                    input = <input type="file" ref={this.fileInput} style={{ width: "90px" }} onChange={(e) => that._onDialogFileUpload(e.target.files)} />
                                }
                            }
                            else {
                                input = <input type="file" ref={this.fileInput} onChange={(e) => that._onDialogFileUpload(e.target.files, item.status)} />
                            }
                            return (
                                <div>
                                    <Stack vertical>
                                        {input}
                                        <Stack horizontal style={{ marginTop: "10px" }} >
                                            {progress}
                                            {text}
                                        </Stack>
                                    </Stack>

                                </div>);

                        }
                    }
                }
            },
            {
                name: 'Action',
                key: 'action',
                fieldName: 'action',
                minWidth: 50,
                maxWidth: 100,
                onRender: (item) => {
                    if (item.state == 1) {
                        return <PrimaryButton onClick={() => { this._onStatusCompleteClicked(item.status) }} text="Complete" />
                    } else if (item.state == 2) {
                        return <DefaultButton text="Pending" disabled={true} />
                    } else {
                        return <DefaultButton text="Completed" disabled={true} />
                    }
                }
            }
        ];
    }

    // Network Calls

    uploadFile(file, status) {
        var storage = firebase.storage();
        var storageRef = storage.ref();
        var leadId = this.props.leadDetails.leadId;
        var newStatus = status;
        var documentId = Date.now();
        var uploadTask = storageRef.child('lead_documents/' + leadId + '/' + newStatus + '/' + documentId + file.name).put(file);
        return uploadTask;
    }

    updateDescription(docid, description) {
        let db = firebase.firestore();
        let ref = db.collection('lead_status_updates').doc(docid);
        ref.update({ description: description }).then(() => {
            let obj = this.state.descriptionEdit;
            obj[docid] = null;
            this.setState({ descriptionEdit: obj });
        });
        let obj = this.state.descriptionEdit;
        obj[docid].status = 1;
        this.setState({ descriptionEdit: obj });
    }

    updateFileUrl(docid, url) {
        let db = firebase.firestore();
        let ref = db.collection('lead_status_updates').doc(docid);

        ref.update({ downloadUrl: firebase.firestore.FieldValue.arrayUnion(url) }).then(() => {
            let obj = this.state.uploadEdit;
            obj[docid] = null;
            this.setState({ uploadEdit: obj });
        });
    }

    changeLeadStatus(leadId, newStatus, description, downloadUrl) {
        var leadStatus = {
            leadid: leadId,
            description: description ?? "",
            status: newStatus,
            downloadUrl: downloadUrl ? [downloadUrl] : [],
            changedon: getCurrentDate()
        };
        var promise1 = firebase.firestore().collection("lead_status_updates").add(leadStatus);
        var promise2 = firebase.firestore().collection("leads").doc(leadId).update({ status: newStatus });
    }

    fetchLeadUpdateStatus(leadId) {
        let that = this;
        let db = firebase.firestore();
        let query = db.collection("lead_status_updates").where('leadid', '==', leadId).orderBy("status");
        query.onSnapshot((snapshot) => {
            let updates = [{ status: 1, changedon: that.props.leadDetails.createdon, state: 0 }];
            let lastStatus = 1;
            snapshot.forEach((doc) => {
                lastStatus = doc.data().status;
                updates.push({ ...doc.data(), id: doc.id, state: 0 });
            });
            let i = lastStatus + 1;
            if (i <= 5) {
                updates.push({
                    status: i,
                    state: 1
                });
            }
            i++;
            while (i <= 5) {
                updates.push({
                    status: i,
                    state: 2
                });
                i++;
            }
            this.setState({ items: updates, upload: 0 });
        });
    }
}

export default LeadStatusDialog;