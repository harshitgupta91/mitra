import React from 'react';
import firebase from 'firebase';
import { ShimmeredDetailsList, SelectionMode, DetailsList, DetailsListLayoutMode, TextField, Stack, Text, Dropdown } from 'office-ui-fabric-react';

class GrantPermission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            _allItems: [],
            columns: this.getColumns(),
            filterPincodes: pincodes
        };
        this.fetchUsers();
    }

    render() {
        return (
            <Stack vertical style={{ textAlign: "left", margin: "10px" }}>
                <Stack horizontal tokens={{ childrenGap: '5%' }}>
                    <TextField label="Search by name" onChange={this._onChangeText.bind(this)} styles={{ fieldGroup: { width: 200 } }} />
                    <TextField label="Search by email" onChange={this._onChangeEmail.bind(this)} styles={{ fieldGroup: { width: 200 } }} />
                    <TextField label="Filter Pincodes" onChange={this._onChangePincode.bind(this)} styles={{ fieldGroup: { width: 200 } }} />
                </Stack>
                {!this.state.items.length ? (
                    < ShimmeredDetailsList
                        setKey="items"
                        items={this.state.items}
                        compact={false}
                        columns={this.state.columns}
                        selectionMode={SelectionMode.none}
                        enableShimmer={true}
                        listProps={{ renderedWindowsAhead: 0, renderedWindowsBehind: 0 }}
                    />) : (
                        <DetailsList
                            items={this.state.items}
                            compact={false}
                            columns={this.state.columns}
                            selectionMode={SelectionMode.none}
                            getKey={this._getKey}
                            setKey="none"
                            layoutMode={DetailsListLayoutMode.justified}
                            isHeaderVisible={true}
                        />)
                }
            </Stack>
        )
    }

    _onColumnClick(ev, column) {
        const newColumns = this.state.columns;
        const currColumn = newColumns.filter(currCol => column.key === currCol.key)[0];
        newColumns.forEach((newCol) => {
            if (newCol === currColumn) {
                currColumn.isSortedDescending = !currColumn.isSortedDescending;
                currColumn.isSorted = true;
            } else {
                newCol.isSorted = false;
                newCol.isSortedDescending = true;
            }
        });
        const newItems = this._copyAndSort(this.state.items, currColumn.fieldName, currColumn.isSortedDescending);
        this.setState({
            columns: newColumns,
            items: newItems
        });
    };

    _onChangeText(ev, text) {
        this.setState({
            items: text ? this.state._allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1) : this.state._allItems
        });
    };

    _onChangeEmail(ev, text) {
        this.setState({
            items: text ? this.state._allItems.filter(i => i.email.toLowerCase().indexOf(text) > -1) : this.state._allItems
        });
    };

    _onChangePincode(ev, text) {
        this.setState({
            filterPincodes: text ? pincodes.filter(i => i.key.indexOf(text) > -1) : pincodes,
            columns: this.getColumns()
        });
    };

    _copyAndSort(items, columnKey, isSortedDescending) {
        const key = columnKey;
        return items.slice(0).sort((a, b) => ((isSortedDescending ? String(a[key]).toLowerCase() < String(b[key]).toLowerCase() : String(a[key]).toLowerCase() > String(b[key]).toLowerCase()) ? 1 : -1));
    }

    _onPincodeDropdownChange(id, option, oldPincodes) {
        var arr = oldPincodes ? oldPincodes.split(',') : [];
        if (option.selected) {
            arr.push(option.key);
        } else {
            var index = arr.indexOf(option.key);
            if (index > -1) {
                arr.splice(index, 1);
            }
        }
        var updates = {};
        updates["/user/" + id + "/pincode"] = arr.join();
        this.updateUser(updates);
    }

    _onUserDropdownChange(id, key) {
        var updates = {};
        updates["/user/" + id + "/userType"] = key
        this.updateUser(updates);
    }

    getColumns() {
        return [
            {
                name: 'Name',
                key: 'name',
                data: 'string',
                fieldName: 'name',
                minWidth: 50,
                maxWidth: 100,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.name}</span>;
                }
            },
            {
                name: 'Email',
                key: 'email',
                data: 'string',
                fieldName: 'email',
                minWidth: 100,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.email}</span>;
                }
            },
            {
                name: 'Created On',
                key: 'createdon',
                data: 'string',
                fieldName: 'createdon',
                minWidth: 100,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return <span>{item.createdon}</span>;
                }
            },
            {
                name: 'User Role',
                key: 'userType',
                data: 'string',
                fieldName: 'userType',
                minWidth: 100,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    return (<Dropdown
                        label=""
                        defaultSelectedKey={item.userType}
                        options={userTypeOptions}
                        disabled={false}
                        onChange={(ev, option) => { this._onUserDropdownChange(item.id, option.key) }}
                    />)
                }
            },
            {
                name: 'Serving Pincode',
                key: 'pincode',
                data: 'string',
                fieldName: 'pincode',
                minWidth: 100,
                maxWidth: 150,
                onColumnClick: this._onColumnClick.bind(this),
                onRender: (item) => {
                    if (item.userType == 2) {
                        return (<Dropdown
                            label=""
                            multiSelect
                            selectedKeys={item.pincode ? item.pincode.split(',') : []}
                            selectedKeys={item.pincode ? item.pincode.split(',') : []}
                            options={this.state.filterPincodes}
                            disabled={false}
                            onChange={(ev, option) => { this._onPincodeDropdownChange(item.id, option, item.pincode) }}
                        />)
                    } else {
                        return <span>All Pincodes</span>
                    }

                }
            }
        ]
    }

    // Network Calls

    updateUser(updateObject) {
        firebase.database().ref().update(updateObject)
            .then(() => {
                this.fetchUsers();
            });
    }

    fetchUsers() {
        let users = [];
        firebase.database().ref("user").orderByChild("userType").startAt(1).once("value").then((snapshot) => {
            snapshot.forEach((data) => {
                users.push(data.val());
            })
            users = this._copyAndSort(users, "name", false);
            this.setState({ items: users, _allItems: users });
        });

    }

}
export default GrantPermission;

const userTypeOptions = [
    { key: 1, text: "No Permission" },
    { key: 2, text: "Sales Manager" },
    { key: 100, text: "Super Admin" },
]

const pincodes = [
    { key: "400001", text: "400001-Andheri" },
    { key: "400002", text: "400002-Andheri 2" },
    { key: "400003", text: "400003-Andheri 3" },
]