export function getCurrentDate() {
    var today = new Date();
    var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return date + ' ' + time;
};

export function getStatus(status) {
    switch (status) {
        case 1:
            return "Lead Generated";
        case 2:
            return "Customer Verified";
        case 3:
            return "Document Collected";
        case 4:
            return "Submitted to Bank";
        case 5:
            return "Card Sent";
        default:
            return null;
    }
}